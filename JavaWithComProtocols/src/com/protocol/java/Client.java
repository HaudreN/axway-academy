package com.protocol.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.net.InetAddress;
import javax.net.SocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;


public class Client {
	public static void main(String[] args) throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException, UnrecoverableKeyException, KeyManagementException {
		SSLContext ctx = SSLContext.getInstance("TLSv1.2");
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SUNX509");
		KeyStore ks= KeyStore.getInstance("JKS");
		ks.load(new FileInputStream(new File("C:\\Users\\DM-20\\Desktop\\JavaRepo\\axway-academy\\JavaWithComProtocols\\ks.jks"))
				, "password".toCharArray());
		kmf.init(ks, "password".toCharArray());
		ctx.init(kmf.getKeyManagers(), null, null);
	PrintWriter writer = null;
	InputStream is = null;
	OutputStream out= null;
	SocketFactory factory = ctx.getSocketFactory();
	try
	{
		Socket socket = factory.createSocket
				(InetAddress.getLoopbackAddress(), 4444);
//		Socket s = new Socket("localhost", 4444);
		 is = socket.getInputStream();
		 out = socket.getOutputStream();
		writer = new PrintWriter(out,true);
		writer.println("Hello Server");

	}catch (UnknownHostException e)
	{
		e.printStackTrace();
	}catch (IOException es)
	{
		es.printStackTrace();
	}finally
	{
		writer.close();
		is.close();
		out.close();
	}

}
}


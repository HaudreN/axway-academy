package com.protocol.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class Server {
 
	public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException, CertificateException {
		SSLContext ctx = SSLContext.getInstance("TLSv1.2");
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SUNX509");
		KeyStore ks= KeyStore.getInstance("JKS");
		ks.load(new FileInputStream(new File("C:\\Users\\DM-20\\Desktop\\JavaRepo\\axway-academy\\JavaWithComProtocols\\ks.jks"))
				, "password".toCharArray());
		kmf.init(ks, "password".toCharArray());
		ctx.init(kmf.getKeyManagers(), null, null);
		
		//EOF SECURITY
		
		InputStream is = null;
		OutputStream out = null;
		ServerSocket serverSocket = null;
		ServerSocketFactory factory = ctx.getServerSocketFactory();
		
		try {
			serverSocket = factory.createServerSocket(4444);
			//ServerSocket socket = new ServerSocket(4444);
			Socket s =serverSocket.accept();
			 is = s.getInputStream();
			 out = s.getOutputStream();
			BufferedReader br= new BufferedReader(new InputStreamReader(is));
			String line;
			while((line =  br.readLine()) !=null)
			{
				if(line.equalsIgnoreCase("end"))
				{
					break;
				}
			System.out.println(line);
		
			}
			}catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			is.close();
		}
	}
}

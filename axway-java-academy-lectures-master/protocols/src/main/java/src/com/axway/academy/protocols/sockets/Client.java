package com.axway.academy.protocols.sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * A client that connects to a server on port 4444 and writes a message.
 * 
 * @author aandreev
 *
 */
public class Client {

    public static void main(String[] args) {
        PrintWriter writer = null;
        InputStream is = null;
        OutputStream out = null;
        Socket s = null;
        try {
            // connecting to the server
            s = new Socket("localhost", 4444);

            // initializing streams for communication
            is = s.getInputStream();
            out = s.getOutputStream();
            writer = new PrintWriter(out);

            // writing a message
            writer.println("Hello Server");
            writer.flush();

        } catch (UnknownHostException e) {
            System.out.println("Problem connecting to server.");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Problem writing to server.");
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (writer != null) {
                    writer.close();
                }
                if (is != null) {
                    is.close();
                }
                if (s != null && s.isConnected()) {
                    s.close();
                }
            } catch (IOException e) {
                System.out.println("Problem closing streams");
                e.printStackTrace();
            }
        }

    }

}

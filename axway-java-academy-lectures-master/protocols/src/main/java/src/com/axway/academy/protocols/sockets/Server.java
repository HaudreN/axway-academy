package com.axway.academy.protocols.sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * A Server working on port 4444. It would accept connections from a single client only.
 * 
 * @author aandreev
 *
 */
public class Server {

    public static void main(String[] args) {
        InputStream is = null;        
        BufferedReader br = null;
        OutputStream out = null;
        ServerSocket serverSocket = null;
        try {
            // defining the port on which our server is going to listen
            serverSocket = new ServerSocket(4444);
            
            // awaiting connections from clients
            Socket s = serverSocket.accept();
            
            // after a client connects to the server
            // the server prints everything the clients says
            is = s.getInputStream();
            out = s.getOutputStream();
            br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }

        } catch (IOException e) {
            System.out.println("Problem with streams.");
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (br != null) {
                    br.close();
                }
                if (is != null) {
                    is.close();
                }
                if (serverSocket != null && !serverSocket.isClosed()) {
                    serverSocket.close();
                }
            } catch (IOException e) {
                System.out.println("Error closing streams.");
                e.printStackTrace();
            }
        }

    }

}

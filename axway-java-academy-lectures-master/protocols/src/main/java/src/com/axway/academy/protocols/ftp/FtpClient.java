package com.axway.academy.protocols.ftp;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

/**
 * An FTP client listing and downloading a file using Apache FTP client. Using a public anonymous FTP server.
 * 
 * @author aandreev
 *
 */
public class FtpClient {

    public static void main(String[] args) {
        // initializing FTP client
        FTPClient ftp = new FTPClient();
        FileOutputStream out = null;
        try {
            // connect to FTP server
            ftp.connect("speedtest.tele2.net");

            // authenticating. Anonymous login - username anonymous and any password
            ftp.login("anonymous", "12312321");

            // list files in user home folder
            FTPFile[] files = ftp.listFiles();
            System.out.println("Found " + files.length + " files.");

            // download a file
            out = new FileOutputStream("1KB.downloaded.zip");
            boolean success = ftp.retrieveFile("1KB.zip", out);
            System.out.println(success ? "File downloaded" : "File not downloaded");
        } catch (IOException e) {
            System.out.println("Problem connecting to FTP server.");
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (ftp != null && ftp.isConnected()) {
                    ftp.disconnect();
                }
            } catch (IOException e) {
                System.out.println("Problem closing streams.");
                e.printStackTrace();
            }
        }
    }

}

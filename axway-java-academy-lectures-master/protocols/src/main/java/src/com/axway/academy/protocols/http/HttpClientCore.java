package com.axway.academy.protocols.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * An HTTP client that executes a GET request using java.net classes.
 * 
 * @author aandreev
 *
 */
public class HttpClientCore {

    public static void main(String[] args) {
        HttpURLConnection connection = null;
        BufferedReader br = null;
        InputStream is = null;
        try {
            // initializing URL
            URL url = new URL("http://google.com");
            
            // opening connection
            connection = (HttpURLConnection) url.openConnection();
            
            // setting request method
            connection.setRequestMethod("GET");
            
            // executing request reading body of the response
            is = connection.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            String responseBody = "";
            String line;
            while ((line = br.readLine()) != null) {
                responseBody = responseBody + line;
            }
            System.out.println(responseBody);

        } catch (MalformedURLException e) {
            System.out.println("Malformed connection URL.");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Problem writing to file.");
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (is != null) {
                    is.close();
                }
                if (connection != null) {
                    connection.disconnect();
                }
            } catch (IOException e) {
                System.out.println("Problem closing streams.");
                e.printStackTrace();
            }

        }

    }

}

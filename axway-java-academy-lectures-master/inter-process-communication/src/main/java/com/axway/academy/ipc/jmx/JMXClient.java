package com.axway.academy.ipc.jmx;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class JMXClient {
    public static void main(String[] args) throws Exception {
        JMXServiceURL jmxServiceURL = new JMXServiceURL(JMXServer.SERVER_SERVICE_URL);
        JMXConnector connector = JMXConnectorFactory
                .newJMXConnector(jmxServiceURL, JMXServer.env);
        connector.connect();
        MBeanServerConnection serverConnection = connector.getMBeanServerConnection();
        ObjectName remoteObjectName = new ObjectName(JMXServer.HELLO_OBJ);
        OperationMBean remoteInstance = JMX.newMBeanProxy(
                serverConnection, remoteObjectName, OperationMBean.class);
        remoteInstance.sayMessage("World!");
    }
}

package com.axway.academy.ipc.process;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProcessDemo {
    public static void main(String[] args) throws Exception {
        ProcessBuilder processBuilder = new ProcessBuilder();
        Process process = processBuilder.command("pwd").start();
        InputStream is = process.getInputStream();
        System.out.println(new String(is.readAllBytes()));

        ProcessBuilder processBuilder1 = new ProcessBuilder();
        Process process1 = processBuilder1.directory(Paths.get(System.getProperty("user.dir"),
                "inter-process-communication/src/main/scripts/").toFile())
                .command(List.of("python3.7", "script.py")).start();
        process1.onExit().thenAcceptAsync(process2 -> {
            try {
                System.out.println(new String(process2.getInputStream().readAllBytes()));
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        });
        System.out.println("The commands are send.");
        TimeUnit.SECONDS.sleep(10);
    }
}

package com.axway.academy.ipc.sockets;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClientDemo {

    public static void main(String[] args) throws Exception {
        try (Socket clientSocket = new Socket("::", SocketServerDemo.SERVER_PORT);
             PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))) {
            System.out.println(String.format("Sending command to server: %s.", SocketServerDemo.READY_COMMAND));
            out.println(SocketServerDemo.READY_COMMAND);
            System.out.println(String.format("Response from server: %s.", in.readLine()));
        }
    }
}

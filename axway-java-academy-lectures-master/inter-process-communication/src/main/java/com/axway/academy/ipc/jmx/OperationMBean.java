package com.axway.academy.ipc.jmx;

public interface OperationMBean {
    void sayMessage(String message);
}

package com.axway.academy.ipc.jmx;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import java.lang.management.ManagementFactory;
import java.rmi.registry.LocateRegistry;
import java.util.Map;

public class JMXServer {

    private static final int SERVER_PORT = 12_000;

    //service:jmx:rmi:///jndi/rmi://localhost:1099/jmxrmi
    static final String SERVER_SERVICE_URL = String.format(
            "service:jmx:rmi:///jndi/rmi://localhost:%d/jmxrmi", SERVER_PORT);

    static final String HELLO_OBJ = "com.axway.academy.Hello:type=basic,name=hello";

    private static final String PROVIDER_URL_VALUE = String.format("rmi://server:%d", SERVER_PORT);

    private static final String INITIAL_CONTEXT_FACTORY_VALUE = "com.sun.jndi.rmi.registry.RegistryContextFactory";

    static final Map<String, String> env = Map.of(
            Context.PROVIDER_URL, PROVIDER_URL_VALUE,
            Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY_VALUE);

    public static void main(String[] args) throws Exception {
        MBeanServer beanServer = ManagementFactory.getPlatformMBeanServer();
        ObjectName objectName = new ObjectName(HELLO_OBJ);
        beanServer.registerMBean(new Operation(), objectName);
        JMXServiceURL url = new JMXServiceURL(SERVER_SERVICE_URL);
        LocateRegistry.createRegistry(SERVER_PORT);
        JMXConnectorServer jmxConnectorServer = JMXConnectorServerFactory
                .newJMXConnectorServer(url, env, beanServer);
        jmxConnectorServer.start();
        while (true){}
    }
}

package com.axway.academy.ipc.rmi.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Remote interface, provided by the server
 */
public interface AddOperationServer extends Remote {
    double add(double arg1, double arg2) throws RemoteException;
}

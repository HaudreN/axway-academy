package com.axway.academy.ipc.rmi.client;

import com.axway.academy.ipc.rmi.server.AddOperationServer;
import com.axway.academy.ipc.rmi.server.Server;

import java.rmi.Naming;

public class Client {
    public static void main(String[] args) throws Exception {
        String serverUrl = String.format("rmi://localhost:%d/%s", Server.RMI_PORT, Server.BIND_NAME);
        AddOperationServer remoteInstance = (AddOperationServer) Naming.lookup(serverUrl);
        System.out.println(String.format("Remote instance called. Result: %f.", remoteInstance.add(2, 3)));
    }
}

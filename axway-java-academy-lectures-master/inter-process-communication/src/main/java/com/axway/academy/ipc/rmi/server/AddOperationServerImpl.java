package com.axway.academy.ipc.rmi.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class AddOperationServerImpl
        extends UnicastRemoteObject
        implements AddOperationServer {

    protected AddOperationServerImpl() throws RemoteException {
    }

    @Override
    public double add(double arg1, double arg2) throws RemoteException {
        return arg1 + arg2;
    }
}

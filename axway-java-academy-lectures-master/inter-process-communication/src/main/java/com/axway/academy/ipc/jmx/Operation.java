package com.axway.academy.ipc.jmx;

public class Operation implements OperationMBean {

    @Override
    public void sayMessage(String message) {
        System.out.println(String.format("Hello: %s.", message));
    }
}

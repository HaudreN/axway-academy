package com.axway.academy;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class DemoFunctionalInterfaces {
    public static void main(String[] args) {
        ArithmeticOperation add = (arg1, arg2) -> {
            int result = arg2 + arg1;
            ArithmeticOperation.print(result);
            return result;
        };
        System.out.println(add.doOperation(3, 4));
        Consumer<String> consumer = System.out::println;
        consumer.accept("Hello");
        Consumer<Integer> consumer2 = (x) -> System.out.println(x*10);
        consumer2
                .andThen(integer -> System.out.println(integer + 10))
                .accept(10);
        Supplier<String> supplier = () -> "New string from supplier.";
        supplier.get();
        Function<String, Integer> func = s -> {
            if (s.contains("1")) {
                return 10;
            }
            return 5;
        };
        Integer result = func.andThen(integer -> integer * integer)
                .compose(Student::getAddress)
                .apply(new Student("Student Name", 23, 3, Student.Degree.BACHELOR, "Sofia 1000"));
        System.out.println(result);
        BiFunction<String, String, Integer> biFunction = (str1, str2) -> Integer.parseInt(str1.concat(str2));
        biFunction.apply("2", "3");
    }
}

package com.axway.academy;

import java.util.Arrays;
import java.util.Optional;
import java.util.StringJoiner;

public class Student {
    private String name;
    private int age;
    private int course;
    private Degree degree;
    private String address;

    public enum Degree {
        BACHELOR("B"),
        MASTER("M"),
        DOCTOR("PHD");
        String shortTitle;
        Degree(String shortTitle) {
            this.shortTitle = shortTitle;
        }
        public static Degree getDegree(String degree) {
            Optional<Degree> first = Arrays.stream(values())
                    .filter(vals -> vals.shortTitle.equals(degree))
                    .findFirst();
            if (first.isPresent()) {
                return first.get();
            }
            throw new IllegalArgumentException("");
        }
    }

    public Student(String name, int age, int course, Degree degree, String address) {
        this.name = name;
        this.age = age;
        this.course = course;
        this.degree = degree;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public Degree getDegree() {
        return degree;
    }

    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Student.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("age=" + age)
                .add("course=" + course)
                .add("degree=" + degree)
                .add("address='" + address + "'")
                .toString();
    }
}

package com.axway.academy;

@FunctionalInterface
public interface ArithmeticOperation {
    double doOperation(int arg1, int arg2);

    static void print(Object obj) {
        System.out.println(obj);
    }

    default void printDouble(double arg1) {
        System.out.println(String.format("From printDouble(): %f.", arg1));
    }
}

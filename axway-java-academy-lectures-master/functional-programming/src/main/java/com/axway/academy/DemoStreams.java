package com.axway.academy;

import java.util.HashMap;
import java.util.List;
import java.util.OptionalInt;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DemoStreams {
    public static void main(String[] args) {
        List<String> list = List.of(
                "Hristo Todorov,25,1,B,Sofia",
                "Stefan Stefanov,27,3,M,Dobrich",
                "Ivan Ivanov,19,1,B,Plovdiv",
                "Georgi Ivanov,21,3,B,Sofia",
                "Gergana Gerganova,26,2,M,Asenovgrad");
        Stream<Student> studentStream = list.stream()
                .map(entry -> {
                    String[] split = entry.split(",");
                    String name = split[0];
                    int age = Integer.parseInt(split[1]);
                    int course = Integer.parseInt(split[2]);
                    Student.Degree degree = Student.Degree.getDegree(split[3]);
                    String address = split[4];
                    return new Student(name, age, course, degree, address);
                });
        List<Student> collect = studentStream
                .filter(student -> Student.Degree.BACHELOR.equals(student.getDegree()))
                .collect(Collectors.toList());
        collect.forEach(System.out::println);
        System.out.println(collect.stream().mapToInt(Student::getAge).summaryStatistics());
        // iteration using the IntStream
        // find the number of all integers from 1 to 100 000, that has residual of 0 by module 7
        System.out.println(IntStream.range(1, 100_000).filter(value -> value % 7 == 0).count());
        // generate 100 000 integers and filter out only the even ones
        System.out.println(IntStream.generate(() -> ThreadLocalRandom.current().nextInt(100))
                .filter(integer -> integer % 2 == 0)
                .limit(100_000)
                .count());
        // interesting example of iteration - an alternative for 'for' loop
        IntStream.iterate(1, i -> i < 10, i -> i + 1)
                .forEach(System.out::println);
        // find the sum of all numbers from 10 up to 100 000
        OptionalInt reduce = IntStream.range(10, 100_000).reduce(Integer::sum);
        if (reduce.isPresent()) { // always check whether or not the value actual exists
            System.out.println(reduce.getAsInt());
        }
        // advanced collector example
        HashMap<String, Student> studentHashMap = collect.stream().collect(
                HashMap::new,
                (studentMap, student) -> studentMap.put(student.getName(), student),
                HashMap::putAll);
        studentHashMap.forEach((name, student) ->
                System.out.println(String.format("Student: %s, age: %d.", name, student.getAge())));
    }
}

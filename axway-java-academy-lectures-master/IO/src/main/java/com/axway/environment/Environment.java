package com.axway.environment;

public class Environment {
    public static void main(String[] args) {
        // Get all of the system properties
        var properties = System.getProperties();
        for (var property : properties.entrySet()) {
            System.out.println(String.format("Key: %s, value: %s.", property.getKey(), property.getValue()));
        }
        // Setting the system property
        System.setProperty("Property1", "Value1");
        // reading other different types of system properties
        System.setProperty("long", "98219");
        // reading property with expected value type of long
        System.out.println(Long.getLong("long"));
        System.out.println(Long.getLong("NON_EXISTING_LONG_PROPERTY", 9898989));
        System.out.println("=========================================================");
        // iterate over all of the environment variables - have in mind that they are read-only!
        var environments = System.getenv();
        for (var env : environments.entrySet()) {
            System.out.println(String.format("Key: %s, value: %s.", env.getKey(), env.getValue()));
        }
    }
}

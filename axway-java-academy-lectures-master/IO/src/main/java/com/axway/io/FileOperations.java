package com.axway.io;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;

public class FileOperations {
    public static void main(String[] args) throws IOException {
        // Getting current working directory
        String workingDir = System.getProperty("user.dir");
        System.out.println(workingDir);
        // Getting the home directory of the current user
        String homeDir = System.getProperty("user.home");
        System.out.println(homeDir);
        // this file is file that is in the current working directory
        File currentDirFile = new File("file.txt");
        System.out.println(currentDirFile.isAbsolute()); // the file path is not absolute
        System.out.println(currentDirFile.exists()); // checking whether or not the file exists
        // if the file does not exists - create it
        if (!currentDirFile.exists()) {
            Files.createFile(currentDirFile.toPath());
        }

        String tempLocation = System.getProperty("java.io.tmpdir");
        System.out.println(tempLocation);
        // working with temp files
        Path tempFile = Files.createTempFile("file", "tmp");
        // the above code will create a file inside tempLocation
        System.out.println(String.format("Temp file location: %s.", tempFile));
        // delete the temp file
        Files.delete(tempFile);
        // below is the checks for the permission types of the file
        currentDirFile.canExecute();
        currentDirFile.canRead();
        currentDirFile.canWrite();
        currentDirFile.isFile(); // 'true' - the file is a regular file
        currentDirFile.isDirectory(); // 'false' - the file is not a directory
        currentDirFile.isHidden(); // 'false' - the file is not hidden file - it does not start with .
        currentDirFile.getParent(); // return the directory in which this file is located
        currentDirFile.getName(); // gets the name of the file
        // sets the permissions flags for execute, write and read
        currentDirFile.setExecutable(true);
        currentDirFile.setReadable(true);
        currentDirFile.setWritable(true);
        // move (rename) the file.txt to <HOME_DIR>/file-renamed.txt
        File renamed = new File(homeDir, "file-renamed.txt");
        currentDirFile.renameTo(renamed);
        // create a directory, called academy inside the Desktop directory
        File desktop = new File(homeDir, "Desktop");
        File academy = new File(desktop, "academy");
        if (!academy.exists()) {
            Files.createDirectory(academy.toPath());
        }
        // now, move the <HOME_DIR>/file-renamed.txt inside the new dir, also rename the file to file.txt
        Path newFile = Files.move(renamed.toPath(), academy.toPath().resolve("file.txt"));
        Files.delete(newFile); // delete the new file
        // create some files in academy dir
        for (var i = 0; i < 10; i++) {
            var fileName = "file" + i + ".txt";
            if (i % 2 == 0) {
                fileName = fileName.replace(".txt", ".xml");
            } else if (i % 3 == 0) {
                fileName = fileName.replace(".txt", ".json");
            }
            // here we using the Paths.get... in order to resolve the new file path
            Files.createFile(Paths.get(academy.getAbsolutePath(), fileName));
        }
        // listing files inside the academy directory
        for (var file : academy.listFiles()) {
            System.out.println(file);
        }
        System.out.println("Filtering all .txt files.");
        // now, let's filter the files, based on their extension
        for (var file : academy.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".txt");
            }
        })) {
            System.out.println(file); // this will print the filename
        }
        // monitoring academy directory
        WatchService watchService = FileSystems.getDefault().newWatchService();
        WatchKey watchKey = academy.toPath().register(watchService, StandardWatchEventKinds.ENTRY_DELETE);
        while (true) {
            List<WatchEvent<?>> watchEvents = watchKey.pollEvents();
            if (watchEvents == null) {
                continue;
            }
            for (var event : watchEvents) {
                if (event.context() instanceof Path) {
                    System.out.println(String.format("File %s has been deleted.", event.context()));
                }
            }
            watchKey.reset();
        }

    }
}

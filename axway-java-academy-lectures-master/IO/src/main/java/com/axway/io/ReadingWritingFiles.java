package com.axway.io;

import java.io.*;
import java.nio.file.Paths;

public class ReadingWritingFiles {
    public static void main(String[] args) throws IOException {
        String homeDir = System.getProperty("user.home");
        File file = Paths.get(homeDir, "Desktop", "academy", "file.txt").toFile();
        // let's write something in file.txt
        try (OutputStream os = new FileOutputStream(file)) {
            os.write("Hello from OutputStream!".concat(System.lineSeparator()).getBytes());
        }
        try (OutputStream os = new FileOutputStream(file, true)) { // this will append to a file content
            os.write("Hello from OutputStream! Again ...".concat(System.lineSeparator()).getBytes());
        }
        // let's read the file
        try (InputStream is = new FileInputStream(file)) {
            var buffer = new byte[64];
            while (is.read(buffer) != -1) {
                System.out.println(new String(buffer));
            }
        }
        // using PrintWriter
        try (var fileWriter = new FileWriter(file)) {
            // this will truncate the content of the file.txt
            fileWriter.write("Hello from PrintWriter!".concat(System.lineSeparator()));
        }
        try (var fileWriter = new FileWriter(file, true)) {
            // this will append to the content of the file.txt
            fileWriter.write("Hello from PrintWriter! Again ...".concat(System.lineSeparator()));
        }
        // using PrintReader
        try (var fileReader = new FileReader(file)) {
            int character;
            while ((character = fileReader.read()) != -1) {
                System.out.print((char)character);
            }
        }
        // using BufferedWriter
        try (var fileWriter = new FileWriter(file);
             var bufferedWriter = new BufferedWriter(fileWriter)) {
            // this will truncate the content of the file.txt
            bufferedWriter.write("Hello from BufferedWriter!");
            bufferedWriter.newLine();
        }
        // using BufferedWriter
        try (var fileWriter = new FileWriter(file, true);
             var bufferedWriter = new BufferedWriter(fileWriter)) {
            // this will append to the content of the file.txt
            bufferedWriter.write("Hello from BufferedWriter! Again ...");
            bufferedWriter.newLine();
        }
        // using BufferedReader
        try (var fileReader = new FileReader(file);
             var bufferedReader = new BufferedReader(fileReader)) {
            String line;
            while ((line = bufferedReader.readLine())!= null) {
                System.out.println(line);
            }
        }

    }
}

package com.axway.academy.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 * A MySQL connector that executes a SELECT query. First with a statement and then with a prepared statement.
 * 
 * @author aandreev
 *
 */
public class MySQLConnector {

    public static void main(String[] args) {

        Connection conn = null;
        try {
            // registering the driver
            Class.forName("com.mysql.jdbc.Driver");

            // opening connection
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/axway", "root", "123456");

            // creating and executing statement
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM PEOPLE");

            // printing result
            while (result.next()) {
                System.out.println(result.getInt("id") + " " + result.getString("name") + " " + result.getInt("age"));
            }

            // use PreparedStatement this time
            System.out.println("===Prepared Statement===");
            System.out.println("Enter name");
            Scanner scan = new Scanner(System.in);
            String input = scan.nextLine();

            // create the PreparedStatement and execute request
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM PEOPLE where name=?");
            preparedStatement.setString(1, input);
            result = preparedStatement.executeQuery();
            // the below will result in SQL injection if used with Statement. PreparedStatement handles this use case
            // exactly
            // ResultSet result = query.executeQuery("SELECT * FROM PEOPLE where name=\"" + input + "\"");

            // printing the result
            while (result.next()) {
                System.out.println(result.getInt("id") + " " + result.getString("name") + " " + result.getInt("age"));
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (!conn.isClosed()) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

}

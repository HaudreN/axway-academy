package com.axway.academy.multithreading;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class Application {

//	private static int counter = 0;
	private static AtomicInteger counter = new AtomicInteger(0);
	
	public static void main(String[] args) {
		Thread sample1 = new SampleThread("Thread1");
		Thread sample2 = new SampleThread("Thread2");
		sample1.start();
		sample2.start();
	}
	
	private static class SampleThread extends Thread {
		
		public SampleThread(String name) {
			super(name);
		}
		
		@Override
		public void run() {
			for (int i = 0; i < 10; i++) {
//				System.out.println(Thread.currentThread().getName() + " : " + counter.incrementAndGet());
//				counter.incrementAndGet();
//			}
			try (FileWriter fw = new FileWriter(new File("/home/apanayotov/workspace/academy/file" + counter.incrementAndGet()))) {
				fw.append("test");
//				counter++;
			} catch (IOException e) {
				
			}
			}
		}
	}
}

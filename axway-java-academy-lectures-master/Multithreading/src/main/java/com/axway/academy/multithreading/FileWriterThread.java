package com.axway.academy.multithreading;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterThread extends Thread {

	private final String prefix = "/home"
			+ "/apanayotov/tmp";
	private int mStart;
	private int mEnd;
	
	public FileWriterThread(int start, int end) {
		mStart = start;
		mEnd = end;
	}
	
	@Override
	public void run() {
		for (int i = mStart; i < mEnd; i++) {
			try (FileWriter fw = new FileWriter
					(new File(prefix, "file" + i))) {
				fw.write("this is a test string");
			} catch (IOException e) {
				
			}
		}
	}
}

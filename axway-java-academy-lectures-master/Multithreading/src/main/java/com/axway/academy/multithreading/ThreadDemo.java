package com.axway.academy.multithreading;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ThreadDemo {
	
	public static void main(String[] args) {
//		final String dirPrefix = "/home"
//				+ "/apanayotov/tmp";
		long start = System.currentTimeMillis();
//		for (int i = 0; i<100000; i++) {
//			try (FileWriter fw = new 
//					FileWriter(
//					new File(dirPrefix,"file" + i))) {
//				fw.write("this is a test string");
//			} catch (IOException e) {
//				
//			}
//		}
		Thread t1 = new FileWriterThread(0, 20000);
		Thread t2 = new FileWriterThread(20001, 40000);
		Thread t3 = new FileWriterThread(40001, 60000);
		Thread t4 = new FileWriterThread(60001, 80000);
		Thread t5 = new FileWriterThread(80001, 100000);
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		try {
			t1.join();
			t2.join();
			t3.join();
			t4.join();
			t5.join();
		} catch (InterruptedException e) {
			
		}
		System.out.println("Execution took: " +
		(System.currentTimeMillis() - start));
	}
}

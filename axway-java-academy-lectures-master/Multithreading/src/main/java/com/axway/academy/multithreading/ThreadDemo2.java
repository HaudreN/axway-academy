package com.axway.academy.multithreading;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ThreadDemo2 {
	
	private static int counter = 0;
	private static final String prefix = "/home"
			+ "/apanayotov/tmp";
	public static void main(String[] args) {
		Thread t1 = new FWThread("Thread1");
		Thread t2 = new FWThread("Thread2");
		t1.start();
		t2.start();
		
	}
	
	private static class FWThread extends Thread {
		
		public FWThread(String name) {
			super(name);
		}
		
		@Override
		public void run() {
			for (int i = 0; i < 10; i++) {
				try (FileWriter fw = new 
						FileWriter(new 
						File(prefix,
						"file" + counter))) {
				fw.write("asdf");
				counter++;
				} catch (IOException e) {
					
				}
			}
		}
	}
}

package com.academy.axway.database.hibernate;

import java.util.Scanner;

public class HibernateExample {

	public static void main(String[] args) {
		Scanner get = new Scanner(System.in);
		System.out.println("Create a new user");
		
		
		System.out.println("Enter name");
		String name = get.nextLine();
		System.out.println("Enter age");
		int age = Integer.parseInt(get.nextLine());
		System.out.println("Is he vegan?");
		boolean vegan = get.nextLine().equals("1")?true : false;
		
		
		User user = new  User(name,age,vegan);
		UserManager manager = new UserManager();
		manager.addUser(user);
	}
}

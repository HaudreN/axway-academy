package com.academy.axway.database.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="user")
public class User {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long id;
	@Column(name="name")
private String name;
	@Column(name="age")
private int age;
	@Column(name="vegan")
private boolean vegan;

	public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getAge() {
	return age;
}

public void setAge(int age) {
	this.age = age;
}

public boolean isVegan() {
	return vegan;
}

public void setVegan(boolean vegan) {
	this.vegan = vegan;
}
public User(String name, int age, boolean vegan)
{
	this.name = name;
	this.age=age;
	this.vegan=vegan;
}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	

	}

}

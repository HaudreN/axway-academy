package com.academy.axway.database.hibernate;



import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class UserManager {

	private static SessionFactory factory = new Configuration().configure().addAnnotatedClass(User.class).buildSessionFactory();
	
	public UserManager()
	{
		
	}
	public void addUser(User user)
	{
		Session session= null;
		Transaction tx = null;
		try
		{
			session = factory.openSession();
			tx = session.beginTransaction();
			Long id= (Long) session.save(user);
			tx.commit();
			System.out.println("A user with id " + id + " was created");
		}catch(HibernateException e)
		{
			tx.rollback();
			e.printStackTrace();
		}catch(Exception e)
		{
			tx.rollback();
		}
		
	}
	public void updateUser(long id, int newAge)
	{
		Session session= null;
		Transaction tx = null;
		try
		{
			session = factory.openSession();
			tx = session.beginTransaction();
			User user = session.get(User.class, id);
			user.setAge(newAge);
			session.update(user);
			tx.commit();
			System.out.println("A user with id " + id+ " was updated");
		}catch(HibernateException e)
		{
			tx.rollback();
			e.printStackTrace();
		}catch(Exception e)
		{
			tx.rollback();
		}
		
	}
	public List<User> listUser()
	{
		List<User> users=null;
		Session session= null;
		Transaction tx = null;
		try
		{
			session = factory.openSession();
			tx = session.beginTransaction();
			users = (List<User>)session.createQuery("from user").list();  
			tx.commit();
		
		}catch(HibernateException e)
		{
			tx.rollback();
			e.printStackTrace();
		}catch(Exception e)
		{
			tx.rollback();
		}finally
		{
			session.close();
		}
	return users;	
	}
}

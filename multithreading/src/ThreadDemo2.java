import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class ThreadDemo2 {
    private static int counter = 0;

    public static void main(String[] args) {
Thread t1= new Thread("Thread1");
Thread t2 = new Thread("Thread2");
t1.start();
t2.start();
    }

    private static class FWThread extends Thread {
        public FWThread(String name) {
            super(name);
        }


        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                try (FileWriter fw = new FileWriter("testFiles/file" + counter + ".txt")) {
                    fw.write("test string");
                    counter++;
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }
        }
    }
}

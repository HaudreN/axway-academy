import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;


public class Main extends Thread {
    private int mStart;
    private int mEnd;
    public final String path = "testFiles/";

    public Main(int start, int end) {
        mStart = start;
        mEnd = end;
    }

    @Override
    public void run() {
        for (int i = mStart; i < mEnd; i++) {
            try (FileWriter fw = new FileWriter(path + "file" + i + ".txt")) {
                fw.write("test string");
            } catch (IOException e) {
                e.printStackTrace();

            }
        }

    }


    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        Thread t1 = new Main(1, 2500);
        Thread t2 = new Main(2501, 5000);
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        }catch(InterruptedException e)
        {
            e.printStackTrace();
        }

        String fileName = "file.txt";

        //START
        //        //Used to create 10 000 files and write this is a test string into them. Afterwords the time needed for this operation is checked.
        //Used with single threat
//        for (int i = 0; i < 5000; i++) {
//            fileName = "testFiles/" + "file" + i + ".txt";
//
//            try (FileWriter fw = new FileWriter(new File(fileName))) {
//                fw.write("this is a test string");
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
        //END
        try (FileWriter returnFile = new FileWriter("speedResults.txt")) {
            returnFile.write("Execution time is " + (System.currentTimeMillis() - start));
        } catch (IOException e) {
            e.printStackTrace();
        }
//}
//
//
    }
}

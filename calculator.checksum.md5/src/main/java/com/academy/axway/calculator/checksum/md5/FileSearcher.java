package com.academy.axway.calculator.checksum.md5;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileSearcher {
	File dirToSearch;
	String dirPath;
	int depth = 0;
	int counter=0;
	List<File> filesInDir;
	File[] filesExisting;
	ArrayList<Path> pathList = new ArrayList<Path>();
	FileSearcher() {

	}

	public void checkInput() {
		boolean inputFalse = true;
		String tempString = "";
		Scanner get = new Scanner(System.in);
		while (inputFalse) {
			tempString = get.next();
			if (tempString.equals("-dir")) {
				dirPath = get.next();
				dirToSearch = new File(dirPath);
				if (dirToSearch.isDirectory()) {
					inputFalse = false;
				} else {
					System.out.println("Directory does not exist");
				}
			} else {
				System.out.println("Please check your input. -dir not written correctly");
			}
			if (!inputFalse) {
				tempString = get.next();
				if (tempString.equals("-depth")) {
					depth = get.nextInt();
					if (depth < 0) {
						System.out.println("Depth must be a positive number");
					}
				} else {
					System.out.println("Please check your input. -depth not written correctly");
				}
			}
		}
		sortInput(dirPath, depth );
		
	}
	public void sortInput(String dirPath, int depth)
	{


		  try {
//			Files.find(Paths.get(dirPath), depth, (p, bfa) -> bfa.isRegularFile()).forEach(System.out::println);

			Files.find(Paths.get(dirPath), depth, (p, bfa) -> bfa.isRegularFile()).forEach(pathList::add);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  printFiles(); 
	}
	 public byte[] createChecksum(String fileName) throws Exception {
	       InputStream fis =  new FileInputStream(fileName);

	       byte[] buffer = new byte[1024];
	       MessageDigest complete = MessageDigest.getInstance("MD5");
	       int numRead;

	       do {
	           numRead = fis.read(buffer);
	           if (numRead > 0) {
	               complete.update(buffer, 0, numRead);
	           }
	       } while (numRead != -1);

	       fis.close();
	       return complete.digest();
	   }

	   public void getMD5Checksum(String fileName) throws Exception {
	       byte[] b = createChecksum(fileName);
	       String result = "";

	       for (int i=0; i < b.length; i++) {
	           result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
	       }
	       printCsv(result, fileName);
	   }
	   public void printCsv(String result, String fileName)
	   {
		   File temp = new File(fileName);
		   FileWriter fw;
		   String csvLocation = fileName.replace(temp.getName(), "") + "checksum.csv";
		   try {
			fw = new FileWriter(csvLocation);
			fw.write(temp.getName());
			fw.write(result);
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }
	public void printFiles()
	{
		pathList.forEach(System.out::println);
	}
}

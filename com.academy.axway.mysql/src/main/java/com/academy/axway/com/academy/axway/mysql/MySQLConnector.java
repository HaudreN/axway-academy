package com.academy.axway.com.academy.axway.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class MySQLConnector {

	public static void main(String[] args)  {
		Scanner get= new Scanner(System.in);
		System.out.println("What is the password ?");
		String password = get.nextLine();
		System.out.println("What is the local host:");
		String localhost = get.nextLine();
		Connection conn = null;
		String input = get.nextLine();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			 conn = DriverManager.getConnection("jdbc:mysql://localhost:" + localhost + "/axwayDatabase", "root", password);
			PreparedStatement statement =conn.prepareStatement("SELECT * FROM axway  where name=?");
			statement.setString(1, input);
			
			ResultSet result =statement.executeQuery();
			while(result.next())
			{
				System.out.println(result.getInt("id") + " " + result.getString("name") + result.getInt("age"));
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			try {
				if(!conn.isClosed())
				{
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
